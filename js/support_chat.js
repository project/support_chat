/**
 * @file Frontend integration of the SupportChat APIs. Requires jQuery. Also makes use of Bootstrap's CSS classes.
 */
(() => {
  $ = jQuery;

  let SupportChat = {
    settings: drupalSettings,
    state: {
      currentUserID: undefined,
      lastMessageID: 0,
      in_queue: false,
      isQueueOpen: false,
      queue: [],
      chats: [],
      lastReadMessages: {},
      users: {},
      chatting: false,
      chatVisible: false,
      options: {
        'auto_scroll': true,
      },
      chatsBeingClosed: [],
      intervals: {}
    },
    // Works similarly to setState from React BUT does not re-render any components when called.
    setState: function (state) {
      this.state = {
        ...this.state,
        ...state,
      };
    },
    sendMessage: (message) => send(message),
    startChat: (customer) => startChat(customer),
    chatPoll: () => chatPoll(),
    queueStatusPoll: () => queueStatusPoll(),
    enterQueue: () => enterQueue(),
    fetchAndSetQueue: () => fetchAndSetQueue(),
    getQueuePosition: () => getQueuePosition(),
    dequeue: (customer) => dequeue(customer),
    addInterval: (f, t) => {
      let iid = setInterval(f, t);
      SupportChat.setState({
        intervals: {
          ...SupportChat.state.intervals,
          [f.name]: iid,
        }
      });
    },
    removeInterval: (f) => {
      clearInterval(SupportChat.state.intervals[f.name]);
      SupportChat.setState({
        intervals: {
          ...SupportChat.state.intervals,
          [f.name]: undefined,
        }
      });
    },
  };

  // Main function.
  jQuery(document).ready(async function () {
    let uid = undefined;
    if (SupportChat.settings.user.uid < 1) {
      // Get session
      uid = await getSession();
    } else {
      uid = SupportChat.settings.user.uid;
    }

    let lastReadMessages = isJsonString(localStorage.getItem('support_chat_last_read_messages'))
      ? JSON.parse(localStorage.getItem('support_chat_last_read_messages'))
      : {};

    SupportChat.setState({
      currentUserID: uid,
      lastReadMessages: lastReadMessages
    });

    if (SupportChat.settings.settings.isSupportRep === true) {
      // Invoke immediately, then set an interval.
      generateQueueList();
      SupportChat.addInterval(generateQueueList, SupportChat.settings.settings.queueRefreshRate * 1000);

      if (SupportChat.settings.settings.available === true) {
        jQuery('.deactivate-user').removeClass('hidden');
      } else {
        jQuery('.activate-user').removeClass('hidden');
      }

      jQuery('.close-queue').on('click', e => toggleQueueClick(e, "close"));
      jQuery('.open-queue').on('click', e => toggleQueueClick(e, "open"));
      jQuery('.activate-user').on('click', e => toggleAvailabilityClick(e, "available"));
      jQuery('.deactivate-user').on('click', e => toggleAvailabilityClick(e, "unavailable"));
      jQuery('.transfer-chat').on('click', e => transferChatClick(e));
    } else {
      jQuery('.enter-queue').on('click', function (e) {
        enterQueueClick(e);
      });
    }
    jQuery('#close-chat-confirm-button').on('click', e => closeChatConfirmClick(e));

    pollAndUpdateQueueStatus();
    SupportChat.addInterval(pollAndUpdateQueueStatus, SupportChat.settings.settings.queueRefreshRate * 1000);
    SupportChat.addInterval(pollAndGenerateChats, SupportChat.settings.settings.refreshRate * 1000);

    jQuery('.subpanel_toggle').on('click', function (e) {
      if (SupportChat.state.chatVisible) {
        jQuery('.subpanel').hide();
        SupportChat.state.chatVisible = false;
      } else {
        jQuery('.subpanel').show();
        SupportChat.state.chatVisible = true;
      }

      return false;
    });
  });

  // Generate components.
  function generateQueueList() {
    SupportChat.fetchAndSetQueue().then(() => {
      if (SupportChat.settings.settings.screeningRequired) {
        jQuery('#support_chat #queue table').removeClass('no-screening');
      }
      createQueueTable(SupportChat.state.queue);

      jQuery(".queue-start-chat").on("click", (e) => {
        let customer = jQuery(e.target).attr('data-user-id');

        SupportChat.startChat(customer);
        dequeue(customer).then(() => generateQueueList());
      });

      jQuery(".queue-remove").on("click", (e) => {
        let customer = jQuery(e.target).attr('data-user-id');
        dequeue(customer).then(() => generateQueueList());
      });
    });
  }

  function pollAndGenerateChats() {
    SupportChat.chatPoll().then(() => {
      let maxMessageID = SupportChat.state.lastMessageID;
      for (let chat of SupportChat.state.chats) {

        chat.user = SupportChat.state.users[chat.other_user_id];
        if (jQuery(`#chat-${chat.id}`).length < 1) {
          if (SupportChat.state.chatsBeingClosed.includes(chat.id)) {
            continue;
          }

          appendChat(chat, SupportChat.settings.settings.isSupportRep);
          // If we are on the queue tab, activate a chat. On page load, will be the first chat of the list. On chat start, will activate the newest chat.
          if (jQuery(`#support_chat .nav li.active > a#queue-link`).length > 0) {
            jQuery(`#support_chat .nav li > a#chat-${chat.id}-link`).click();
          }
        } else if (jQuery(`#chat-${chat.id} .closed-message`).length > 0) {
          // This check will catch chats that were previously transferred to another user and have been transferred back.
          jQuery(`#chat-${chat.id} .closed-message`).remove();
          jQuery(`#chat-${chat.id} .chatbox-textarea`).prop('disabled', false);
        }

        if (chat.messages.length === 0) {
          continue;
        }

        let $chatbox_messages = jQuery(`#chat-${chat.id}.chatbox .chatbox-messages`);
        let elementHeight = $chatbox_messages.height();
        let scrollHeight = $chatbox_messages.prop('scrollHeight');
        let scrollTop = $chatbox_messages.scrollTop();
        let lastMessageHeight = $chatbox_messages.children().last().height();
        let unreadMessages = 0;

        for (let message of chat.messages) {
          message.senderUsername = message.sender === SupportChat.state.currentUserID ? 'You' : SupportChat.state.users[message.sender];
          if (parseInt(message.id) > parseInt(maxMessageID)) {
            maxMessageID = message.id;
          }
          if (jQuery(`#message-${message.id}`).length < 1) {
            appendMessage(message, chat.id);
            if ((SupportChat.state.lastReadMessages[chat.id] === undefined || message.id > SupportChat.state.lastReadMessages[chat.id])
              && message.senderUsername !== 'You') {
              unreadMessages++;
            }
          }
        }

        if (scrollTop > (scrollHeight - lastMessageHeight - elementHeight)) {
          // Animated scroll to newest message
          $chatbox_messages.animate({ scrollTop: $chatbox_messages.prop('scrollHeight') }, 500);
        }

        // SPECIFIC to each chat returned during polling
        updateMessageCount(chat, unreadMessages);
      }

      // GENERAL to the chat application.
      notificationOperations();

      SupportChat.setState({
        lastMessageID: maxMessageID
      })
    })
  }

  const updateMessageCount = (chat, unreadMessages = 0) => {
    let $chatbox_messages = jQuery(`#chat-${chat.id}.chatbox .chatbox-messages`);
    let newLastMessageHeight = $chatbox_messages.children().last().height();
    let currentScrollPosition = $chatbox_messages.scrollTop() + $chatbox_messages.innerHeight() + newLastMessageHeight;

    if (unreadMessages > 0 &&
      (currentScrollPosition < $chatbox_messages.prop('scrollHeight')
        || !jQuery(`#chat-${chat.id}`).hasClass('active')
        || jQuery(`#chat-${chat.id}`).is(":hidden"))
    ) {
      let currentUnread = parseInt(jQuery(`#chat-${chat.id}-link .unread-count`).html());
      if (!isNaN(currentUnread)) {
        unreadMessages += currentUnread;
      }
      jQuery(`#chat-${chat.id}-link .unread-count`).html(unreadMessages);
      jQuery(`#chat-${chat.id}-link .unread-count`).removeClass('hidden');
    } else if (currentScrollPosition >= $chatbox_messages.prop('scrollHeight')) {
      let newestMessageID = $chatbox_messages.children().last().prop('id').replace('message-', '');
      jQuery(`#chat-${chat.id}-link .unread-count`).html('');
      jQuery(`#chat-${chat.id}-link .unread-count`).addClass('hidden');

      SupportChat.setState({
        lastReadMessages: {
          ...SupportChat.state.lastReadMessages,
          [chat.id]: newestMessageID,
        }
      });
    }
  }

  const updateCustomerUnreadFlag = () => {
    let $customerChatPanel = jQuery("#support_chat #customer-chat-panel");
    if ($customerChatPanel.length > 0) {
      let totalUnreadMessages = 0;
      for (let $tab of jQuery("#support_chat .nav .unread-count")) {
        let msgCount = parseInt(jQuery($tab).html());
        if (!isNaN(msgCount)) {
          totalUnreadMessages += msgCount;
        }
      }

      if (totalUnreadMessages > 0) {
        if ($customerChatPanel.is(":hidden")) {
          jQuery("#support_chat .subpanel_title_text .unread-flag").removeClass('hidden');
        }
      } else {
        jQuery("#support_chat .subpanel_title_text .unread-flag").addClass('hidden');
      }
    }
  };

  const updateActiveChatNotifications = () => {
    let $activeChatbox = jQuery('.chatbox.active');
    if ($activeChatbox.length > 0 && !$activeChatbox.is(":hidden")) {
      if ($activeChatbox.prop('id') !== undefined && $activeChatbox.prop('id') !== '') {
        let chat_id = $activeChatbox.prop('id').replace('chat-', '');
        let chat = SupportChat.state.chats.find((chat) => chat.id === chat_id);
        if (chat !== undefined) {
          updateMessageCount(chat);
        }
      }
    }
  };

  const notificationOperations = () => {
    updateActiveChatNotifications();
    if (!SupportChat.settings.settings.supportRep) {
      updateCustomerUnreadFlag();
    }
    localStorage.setItem('support_chat_last_read_messages', JSON.stringify(SupportChat.state.lastReadMessages));
  };

  function pollAndUpdateQueueStatus() {
    SupportChat.queueStatusPoll().then(() => {
      if (SupportChat.settings.settings.isSupportRep) {
        if (SupportChat.state.isQueueOpen === true) {
          jQuery('.close-queue').removeClass('hidden');
        } else {
          jQuery('.open-queue').removeClass('hidden');
        }
      } else {
        if (!SupportChat.state.isQueueOpen) {
          jQuery("#support_chat #queue .queue-wrapper").html(`<p class="queue-closed-flag">${SupportChat.settings.settings.queueClosedMessage}</p>`);
        } else if (jQuery('.queue-closed-flag').length > 0) {
          jQuery(`#support_chat #queue .queue-wrapper`).html(`<a class="btn btn-primary enter-queue">Enter Queue</a>`);
          jQuery('#support_chat #queue a.enter-queue').prop('disabled', false);
          jQuery('.enter-queue').on('click', function (e) {
            enterQueueClick(e);
          });
        }
      }
    });
  }

  function checkQueuePosition() {
    SupportChat.getQueuePosition().then((data) => {
      if (data.position === null) {
        SupportChat.removeInterval(checkQueuePosition);

        // This check makes sure that the queue text has not already been replaced by the chatPoll function, which would indicate that a chat has been initiated.
        if (jQuery("#queue-position").length > 0) {
          jQuery(`#support_chat #queue .queue-wrapper`).html(`<p>You have been removed from the queue.</p><a class="btn btn-primary enter-queue">Enter Queue</a>`);
          jQuery('#support_chat #queue a.enter-queue').prop('disabled', false);
          jQuery('.enter-queue').on('click', function (e) {
            enterQueueClick(e);
          })
        }
      } else {
        jQuery("#queue-position").html(data.position);
      }
    });
  }

  async function chatPoll() {
    let chatData = await getPoll(SupportChat.state.lastMessageID);
    let isCustomer = !SupportChat.settings.settings.isSupportRep;
    let lastReadMessages = { ...SupportChat.state.lastReadMessages };

    if (chatData.chats.length < SupportChat.state.chats.length) {
      for (let chat of SupportChat.state.chats) {
        if (chatData.chats.find(cd => cd.id === chat.id) === undefined) {
          jQuery(`#chat-${chat.id} .chatbox-messages`).append(`<p class="text-danger closed-message">Chat closed.</p>`);
          jQuery(`#chat-${chat.id} .chatbox-textarea`).prop('disabled', true);
          delete lastReadMessages[chat.id];
        }
      }

      if (isCustomer) {
        if (SupportChat.state.intervals['pollAndUpdateQueueStatus'] === undefined) {
          SupportChat.addInterval(pollAndUpdateQueueStatus, SupportChat.settings.settings.queueRefreshRate * 1000);
        }
        jQuery(`#support_chat #queue .queue-wrapper`).html(`<a class="btn btn-primary enter-queue">Enter Queue</a>`);
        jQuery('#support_chat #queue a.enter-queue').prop('disabled', false);
        jQuery('.enter-queue').on('click', function (e) {
          enterQueueClick(e);
        });
      }
    }

    if (isCustomer) {
      if (chatData.chats.length > 0) {
        if (SupportChat.state.intervals['checkQueuePosition'] !== undefined) {
          SupportChat.removeInterval(checkQueuePosition);
        }
        jQuery(`#support_chat #queue .queue-wrapper`).html(`<p>Cannot enter queue because a chat is currently active.</p>`);
      } else {
        if (jQuery('#support_chat #queue a.enter-queue').attr('disabled')) {
          jQuery('#support_chat #queue a.enter-queue').prop('disabled', false);
          jQuery('#support_chat #queue a.enter-queue').removeAttr('disabled');
        }
      }
    }

    SupportChat.setState({
      chats: chatData.chats,
      users: chatData.users,
      lastReadMessages: lastReadMessages
    });
  }

  async function send(message) {
    let response = await postMessage(message);

    pollAndGenerateChats();
  }

  async function enterQueue(customer, screeningResponse = undefined) {
    let response;
    if (screeningResponse !== undefined) {
      response = await enqueueWithScreening(customer, screeningResponse);
    } else {
      response = await enqueue(customer);
    }

    if (response.isOpen === false) {
      pollAndUpdateQueueStatus();
      if (SupportChat.state.intervals['pollAndUpdateQueueStatus'] === undefined) {
        SupportChat.addInterval(pollAndUpdateQueueStatus, SupportChat.settings.settings.queueRefreshRate * 1000);
      }
    } else {
      jQuery("#support_chat #queue .queue-wrapper").html(`
          <p>
              Please wait for a support representative to begin chat.
              <br>
              <br>
              Position in queue: <span id="queue-position"></span>
          </p>`);
      SupportChat.removeInterval(pollAndUpdateQueueStatus);
      SupportChat.addInterval(checkQueuePosition, SupportChat.settings.settings.refreshRate * 1000);
    }
  }

  async function startChat(customer) {
    let chat = await postStart(customer, parseInt(SupportChat.state.currentUserID));

    if (chat.chat_id !== undefined && chat.chat_id > 0) {
      pollAndGenerateChats();
    }
  }

  async function queueStatusPoll() {
    let status = await getQueueStatus();

    if (status.open === true) {
      SupportChat.setState({
        isQueueOpen: true
      });
    } else {
      SupportChat.setState({
        isQueueOpen: false
      });
    }
  }

  async function fetchAndSetQueue() {
    let queue = await getEnumerateQueue();

    SupportChat.setState({
      queue: queue
    });
  }

  // Request functions
  // TODO: Add error handling to request functions.
  async function getSession() {
    let results = await $.ajax({
      url: `/chat-api/session`
    });
    // Error: Kill everything. (?)

    return results.sid;
  }

  async function getPoll(messageID) {
    let results = await $.ajax({
      url: `/chat-api/poll/${messageID}`
    });
    // Error: Add to an error counter; fail out with message after 3-5 attempts.

    return results;
  }

  async function getEnumerateQueue() {
    let results = await $.ajax({
      url: "/chat-api/enumerate-queue"
    });
    // Error: "Cannot get queue"

    return results.records;
  }

  async function getQueueStatus() {
    let results = await $.ajax({
      url: "/chat-api/queue-status"
    });
    // Error: "Cannot get the status of queue"

    return results;
  }

  async function getQueuePosition() {
    let results = await $.ajax({
      url: "/chat-api/queue-position"
    });
    // Error: "Cannot get the position of queue"

    return results;
  }

  async function getAvailableSupportReps() {
    let results = await $.ajax({
      url: "/chat-api/available-support-reps"
    });
    // Error: "Unable to retrieve available support reps"

    return results;
  }

  async function enqueue(customer) {
    let results = await $.ajax({
      url: "/chat-api/enqueue",
      method: "POST",
      data: JSON.stringify({
        "customer": customer
      })
    });
    // Error: "Failed to enter queue; please try again."

    return results;
  }

  async function enqueueWithScreening(customer, screeningResponse) {
    let results = await $.ajax({
      url: "/chat-api/enqueue",
      method: "POST",
      data: JSON.stringify({
        "customer": customer,
        "screening_response": screeningResponse
      })
    });
    // Error: Same as enqueue

    return results;
  }

  async function dequeue(customer) {
    let results = await $.ajax({
      url: "/chat-api/dequeue",
      method: "DELETE",
      data: JSON.stringify({
        "customer": customer
      })
    });
    // Error: "Could not remove user from queue"

    return results;
  }

  async function postStart(customer, supportRep) {
    let results = await $.ajax({
      url: "/chat-api/start",
      method: "POST",
      data: JSON.stringify({
        "customer": customer,
        "support_rep": supportRep
      })
    });
    // Error: "Could not start chat"

    return results;
  }

  async function postMessage(message) {
    let results = await $.ajax({
      url: "/chat-api/send",
      method: "POST",
      data: JSON.stringify({
        "chat_id": parseInt(message.chat_id),
        "sender": message.sender,
        "content": message.content
      })
    });
    // Error: Failed to send message

    return results;
  }

  async function patchClose(chatID) {
    SupportChat.setState({
      chatsBeingClosed: [...SupportChat.state.chatsBeingClosed, chatID]
    });
    let results = await $.ajax({
      url: "/chat-api/close",
      method: "PATCH",
      data: JSON.stringify({
        "chat_id": parseInt(chatID)
      })
    });

    SupportChat.setState({
      chatsBeingClosed: SupportChat.state.chatsBeingClosed.filter((id) => id !== chatID)
    });
    // Error: Could not close chat

    return results;
  }

  async function patchCloseQueue() {
    let results = await $.ajax({
      url: "/chat-api/close-queue",
      method: "PATCH"
    });
    // Error: Could not close queue

    return results;
  }

  async function patchOpenQueue() {
    let results = await $.ajax({
      url: "/chat-api/open-queue",
      method: "PATCH"
    });
    // Error: Could not enter queue

    return results;
  }

  async function patchTransfer(chatID, supportRep) {
    let results = await $.ajax({
      url: "/chat-api/transfer",
      method: "PATCH",
      data: JSON.stringify({
        "chat_id": parseInt(chatID),
        "support_rep": parseInt(supportRep)
      })
    });
    // Error: Could not transfer chat {chatID}

    return results;
  }

  async function patchAvailable() {
    let results = await $.ajax({
      url: "/chat-api/available",
      method: "PATCH"
    });
    // Error: Could not go available

    return results;
  }

  async function patchUnavailable() {
    let results = await $.ajax({
      url: "/chat-api/unavailable",
      method: "PATCH"
    });
    // Error: Could not go unavailable

    return results;
  }

  // Event Handlers
  const textareaSubmit = (e) => {
    if (e.keyCode === 13 && e.shiftKey === false) {
      e.preventDefault();

      let chatID = jQuery(e.target).attr('data-chat-id');
      jQuery(`#chat-${chatID} .submit-button`).click();
    }
  };

  const messageSubmit = (e) => {
    let $target = jQuery(e.target);
    let chatID = $target.attr('data-chat-id');
    let $textarea = jQuery(`#chat-${chatID} .chatbox-textarea`);
    let message = $textarea.val();
    message = message.replace(/^\s+|\s+$/g, "");

    $textarea.val('');
    $textarea.focus();
    $textarea.css('height', '44px');

    if (message !== '' && message !== undefined) {
      send({ sender: SupportChat.state.currentUserID, content: message, chat_id: chatID });
    }
  };

  const closeChatClick = (e) => {
    let chatID = jQuery(e.target).attr('data-chat-id');
    jQuery('#close-chat-confirm-button').data('chat-id', chatID);
    jQuery(`#close-chat-confirmation`).modal('show');
  };

  const closeChatConfirmClick = (e) => {
    let chatID = $(e.target).data('chat-id');
    if (chatID) {
      patchClose(chatID).then(closeChat(chatID));
    }
  }

  const enterQueueClick = (e) => {
    if (e.target.disabled === false) {
      if (SupportChat.settings.settings.screeningRequired) {
        screenCustomer(SupportChat.settings.settings.screeningQuestion, SupportChat.state.currentUserID);
      } else {
        enterQueue(SupportChat.state.currentUserID);
      }
    }
  };

  const toggleQueueClick = (e, openClose) => {
    if (openClose === "close") {
      patchCloseQueue().then(() => {
        jQuery(".close-queue").addClass("hidden");
        jQuery(".open-queue").removeClass("hidden");
      });
    } else {
      patchOpenQueue().then(() => {
        jQuery(".open-queue").addClass("hidden");
        jQuery(".close-queue").removeClass("hidden");
      });
    }
  };

  const toggleAvailabilityClick = (e, newAvailability) => {
    if (newAvailability === "available") {
      patchAvailable().then(() => {
        jQuery(".activate-user").addClass("hidden");
        jQuery(".deactivate-user").removeClass("hidden");
      });
    } else if (newAvailability === "unavailable") {
      patchUnavailable().then(() => {
        jQuery(".deactivate-user").addClass("hidden");
        jQuery(".activate-user").removeClass("hidden");
      });
    }
  };

  const transferChatClick = (e) => {
    let repID = jQuery(e.target).attr('data-support-rep-id');
    let chatID = jQuery(e.target).attr('data-chat-id');

    if (repID !== '' && repID !== undefined && chatID !== '' && chatID !== undefined) {
      patchTransfer(chatID, repID).then(() => {
        jQuery(e.target).attr('data-support-rep-id', '');
        jQuery(e.target).attr('data-chat-id', '');
        jQuery(`#transfer-chat-window`).modal('hide');
      });
    }
  };

  const openTransferClick = (e) => {
    getAvailableSupportReps().then((data) => {
      jQuery(`#transfer-chat-window #active-reps-list`).html(formattedTransferUserList(data.users, SupportChat.state.currentUserID));
      jQuery(`#transfer-chat-window #active-reps-list .transfer-user .transfer-rep-radio`).on('click', transferUserSelect);
      jQuery(`#transfer-chat-window .transfer-chat`).attr('data-chat-id', jQuery(e.target).attr('data-chat-id'));
      jQuery(`#transfer-chat-window .transfer-chat`).attr('data-support-rep-id', '');
      jQuery(`#transfer-chat-window .transfer-chat`).attr('disabled', true);
      jQuery(`#transfer-chat-window`).modal('show');
    });
  };

  const transferUserSelect = (e) => {
    let repID = jQuery(e.target).attr('data-support-rep-id');
    jQuery(`#transfer-chat-window .transfer-chat`).attr('data-support-rep-id', repID);
    jQuery(`#transfer-chat-window .transfer-chat`).attr('disabled', false);
  };

  // HTML Output Functions
  function createQueueTable(queue) {
    let $queueBody = jQuery('#support_chat #queue #queue-list');
    $queueBody.html("");
    if (queue.length > 0) {
      let users = queue.sort((a, b) => parseInt(a.timestamp) - parseInt(b.timestamp));

      for (let user of users) {
        $queueBody.append(formattedQueueUser(user));
      }
    } else {
      $queueBody.append(`<tr class="no-users-in-queue"><td colspan="4" class="text-center">No users currently in queue.</td></tr>`);
    }
  }

  function appendChat(chat, isSupportRep = false) {
    jQuery('#support_chat .nav').append(formattedChatTabLink(chat));
    jQuery('#support_chat .tab-content').append(formattedChatTabContent(chat.id, isSupportRep));
    jQuery(`#support_chat #chat-${chat.id} .submit-button`).on('click', messageSubmit);
    jQuery(`#support_chat #chat-${chat.id} .chatbox-textarea`).on('keydown', textareaSubmit);
    jQuery(`#support_chat #chat-${chat.id} .close-chat`).on('click', closeChatClick);
    jQuery(`#support_chat #chat-${chat.id} .show-transfer`).on('click', openTransferClick);
  }

  function appendMessage(message, chatID) {
    jQuery(`#support_chat #chat-${chatID}.chatbox .chatbox-messages`).append(formattedMessage(message));
  }

  const closeChat = (chatID) => {
    jQuery(`#chat-${chatID}`).remove();
    jQuery(`#chat-${chatID}-link`).remove();
    jQuery('#support_chat .nav li:first-child > a').click();
  };

  const screenCustomer = (screeningQuestion, userID) => {
    jQuery(`#support_chat #queue .queue-wrapper`).html(formattedScreeningQuestion(screeningQuestion));
    jQuery("input:radio[name ='screeningOption']").on('change', function (e) {
      jQuery(`#support_chat #queue .queue-wrapper .send-screening`).attr('disabled', false);
    });
    jQuery(`#support_chat #queue .queue-wrapper .send-screening`).on('click', function (e) {
      let screeningResponse = jQuery("input:radio[name ='screeningOption']:checked").val();
      if (screeningResponse !== undefined) {
        enterQueue(userID, screeningResponse);
      } else {
        console.error('No screening response provided.');
      }
    });
  };

  // HTML Templates

  const formattedMessage = (message) => {
    let formatClass = 'left-message';
    if (message.senderUsername === 'You') {
      formatClass = 'right-message';
    }
    return `
  <div class="chatbox-message" id="message-${message.id}">
      <span class="chatbox-message-sender"><strong>${message.senderUsername}</strong></span>
      <span class="chatbox-message-time">${formatChatDatetime(message.timestamp)}</span>
      <p class="chatbox-message-content ${formatClass}">${message.content.replace(/\n/g, '<br>')}</p>
  </div>
    `;
  };

  const formattedChatTabLink = (chat) => {
    return `
  <li>
    <a id="chat-${chat.id}-link" href="#chat-${chat.id}" data-toggle="tab">${chat.user} <span class="unread-count hidden"></span></a>
  </li>
    `;
  };

  const formattedChatTabContent = (chatID, isSupportRep) => {
    return `
  <div id="chat-${chatID}" class="tab-pane chatbox">
    <div class="chatbox-messages"></div>
    <div class="chatbox-input">
        <textarea class="chatbox-textarea" data-chat-id="${chatID}"></textarea>
        <a class="submit-button" data-chat-id="${chatID}"><i class="fa fa-location-arrow" data-chat-id="${chatID}"></i></a>
    </div>
    <a class="close-chat" data-chat-id="${chatID}">End Chat</a>
    ${isSupportRep === true ? `<a class="show-transfer" data-chat-id="${chatID}">Transfer Chat</a>` : ``}
  </div>   
    `;
  };

  const formattedTransferUserList = (users, currentUserID = null) => {
    let result = '';

    for (let userID in users) {
      if (userID !== currentUserID) {
        result += formattedTransferUser(users[userID]);
      }
    }

    if (result === '') {
      return '<span>No other support reps are currently available.</span>';
    }

    return result;
  };

  const formattedTransferUser = (user) => {
    return `
  <div class="transfer-user" >
    <input name="transfer-rep" type="radio" data-support-rep-id="${user.id}" id="transfer-rep-${user.id}" class="transfer-rep-radio" />
    <label for="transfer-rep-${user.id}">${user.username}</label>
  </div>
  `;
  };

  const formattedQueueUser = (user) => {
    return `
  <tr>
    <td>${user.customer_username}</td>
    <td>${user.screening_response}</td>
    <td>${timestampDiff(Date.now() / 1000, user.timestamp)}
    <td>
        <a class="btn btn-secondary queue-remove" data-user-id="${user.customer}">Remove</a>
        <a class="btn btn-primary queue-start-chat" data-user-id="${user.customer}">Chat</a>
    </td>
  </tr>
    `;
  };

  const formattedScreeningQuestion = (screeningQuestion) => {
    return `
  <div id="screening-question">
    <h3>${screeningQuestion.question}</h3>
    ${screeningQuestion.options.reduce((html, option, index) => html + formattedScreeningOption(option, index), ``)}
    <a class="btn btn-primary send-screening" disabled>Send</a>
  </div>
    `;
  };

  const formattedScreeningOption = (option, optionNum) => {
    return `
  <div class="screening-option">
    <input type="radio" id="option${optionNum}" name="screeningOption" value="${option}" />
    <label for="option${optionNum}">${option}</label>
  </div>
    `;
  };

  // UTILITIES
  /**
   * Takes a timestamp and formats it for a chatbox. Date only included if it is any other day than today.
   * @param {int|string} timestamp
   */
  const formatChatDatetime = (timestamp) => {
    let datetime = new Date(parseInt(timestamp) * 1000);
    let now = new Date();

    let result = '';

    if (datetime.toLocaleDateString() !== now.toLocaleDateString()) {
      result += datetime.toLocaleDateString() + ' ';
    }
    result += datetime.toLocaleTimeString();

    return result;
  };

  /**
   * Takes two timestamps in seconds and returns the difference between them in number of hours/minutes.
   * @param {int|string} newer
   * @param {int|string} older
   */
  const timestampDiff = (newer, older) => {
    // Convert timestamps to int.
    newer = parseInt(newer);
    older = parseInt(older);

    // We want positive diffs, so swap the timestamps if necessary.
    if (newer < older) {
      let temp = newer;
      newer = older;
      older = temp;
    }

    let diff = newer - older;

    let hours = Math.floor(diff / 3600);
    let minutes = Math.floor((diff - hours * 3600) / 60);

    let result = ``;
    result += hours > 0 ? `${hours} hour(s) ` : ``;
    result += minutes > 0 ? `${minutes} minute(s)` : ``;
    result = (hours === 0 && minutes === 0) ? `Less than one minute` : result;

    return result;
  };

  /**
   * Takes a string and checks if it is parsable JSON. Returns true/false.
   */
  const isJsonString = (s) => {
    try {
      JSON.parse(s);
    } catch (e) {
      return false;
    }
    return true;
  };
})();
