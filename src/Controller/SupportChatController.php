<?php

namespace Drupal\support_chat\Controller;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\SessionManagerInterface;
use Drupal\encrypt\EncryptServiceInterface;
use Drupal\encrypt\Entity\EncryptionProfile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Validator\Constraints\Type as AssertType;
use Symfony\Component\Validator\Constraints\Collection as AssertCollection;
use Symfony\Component\Validator\Validation;

/**
 * Main API controller for SupportChat functions.
 *
 * All APIs except for the API to toggle the queue are in this class.
 */
class SupportChatController extends ControllerBase {

  private $supportChatConfig;
  private $database;
  private $logger;
  private $sessionManager;
  private $requestStack;
  private $encryptionService;

  /**
   * Constants for names of SupportChat database tables.
   */
  private const SUPPORT_CHAT_CHAT_TABLE = "support_chat_chats";
  private const SUPPORT_CHAT_MSG_TABLE = "support_chat_messages";
  private const SUPPORT_CHAT_QUEUE_TABLE = "support_chat_queue";

  /**
   * Controller constructor.
   */
  public function __construct(Connection $connection,
    LoggerChannelFactoryInterface $loggerFactory,
    SessionManagerInterface $session_manager,
    RequestStack $request_stack,
    EntityTypeManagerInterface $entity_type_manager,
    ?EncryptServiceInterface $encryption_service) {
    $this->supportChatConfig = $this->config('support_chat.settings');
    $this->database = $connection;
    $this->logger = $loggerFactory->get('chat');
    $this->sessionManager = $session_manager;
    $this->requestStack = $request_stack;
    $this->entityTypeManager = $entity_type_manager;
    $this->encryptionService = $encryption_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('session_manager'),
      $container->get('request_stack'),
      $container->get('entity_type.manager'),
      $container->has('encryption') ? $container->get('encryption') : NULL,
    );
  }

  /**
   * @defgroup chat_api_functions Chat API Functions
   * @{
   * Functions used in APIs exposed to client.
   *
   * For access/permissions information, refer to the routing.yml file.
   */

  /**
   * Returns the current session ID. Primarily for anonymous users.
   */
  public function sessionApi() {
    return new JsonResponse([
      'sid' => $this->getSession($this->requestStack->getCurrentRequest()),
    ]);
  }

  /**
   * Queries for a list of active chats/messages for the current user.
   *
   * @param string $message_id
   *   The last message ID received by the current user's client.
   *   The function will only return messages with higher IDs.
   *   Default value: '0'.
   */
  public function poll(string $message_id) {
    $current_user = (int) $this->currentUser()->id();
    if ($current_user === 0) {
      $current_user = $this->getSession($this->requestStack->getCurrentRequest());
    }

    $chats = $this->database->query('SELECT customer, support_rep, id FROM ' . self::SUPPORT_CHAT_CHAT_TABLE . ' WHERE active = 1 AND (customer = :current_user OR support_rep = :current_user)', [
      ':current_user' => $current_user,
    ])->fetchAll();

    if (count($chats) < 1) {
      return new JsonResponse(['chats' => [], 'users' => (object) []]);
    }

    // Getting a unique list of users listed in the chat table.
    $chat_users = array_unique(array_map(function ($chat) {
      return $chat->customer == $this->currentUser()->id() || $this->currentUser()->id() == 0 ? $chat->support_rep : $chat->customer;
    }, $chats));
    $chat_ids = array_map(function ($chat) {
      return $chat->id;
    }, $chats);

    $messages = $this->database->query('SELECT id, content, chat_id, sender, timestamp FROM ' . self::SUPPORT_CHAT_MSG_TABLE . ' WHERE chat_id IN (:chats[]) AND id > :messageID', [
      ':chats[]' => $chat_ids,
      ':messageID' => $message_id,
    ])->fetchAll();

    $senders = $this->database->query('SELECT DISTINCT sender FROM ' . self::SUPPORT_CHAT_MSG_TABLE . ' WHERE chat_id IN (:chats[]) AND sender != :current_user AND sender NOT IN (:chat_users[])', [
      ':chats[]' => $chat_ids,
      ':current_user' => $current_user,
      ':chat_users[]' => $chat_users,
    ])->fetchAll();

    $senders = array_map(function ($sender) {
      return $sender->sender;
    }, $senders);
    $users = array_merge($chat_users, $senders);

    // Build assoc array to be output as JSON object with user IDs as keys.
    $users_with_names = [];
    foreach ($users as $user) {
      $users_with_names[$user] = preg_match("/[a-z]+/i", $user) > 0
        ? 'Anonymous ' . substr($user, 0, 5)
        : $this->entityTypeManager->getStorage('user')->load(intval($user))
        ->getUsername();
    }

    $chat_output = [];
    // Map chat IDs to an index for easier nesting of messages.
    $chat_id_map = [];
    for ($i = 0; $i < count($chats); $i++) {
      $chat = $chats[$i];
      $chat_id_map[$chat->id] = $i;
      array_push($chat_output, (object) [
        "id" => $chat->id,
        "other_user_id" => $chat->customer == $current_user ? $chat->support_rep : $chat->customer,
        "messages" => [],
      ]);
    }

    foreach ($messages as $message) {
      $idx = $chat_id_map[$message->chat_id];
      $content = $message->content;

      if ($this->supportChatConfig->get('support_chat_use_encryption') == 1 && $this->encryptionService !== NULL) {
        $encryption_profile = $this->supportChatConfig->get('support_chat_encryption_profile');
        $encryption_profile = $encryption_profile != NULL ? EncryptionProfile::load($encryption_profile) : NULL;

        if ($encryption_profile !== NULL) {
          $content = $this->encryptionService->decrypt($content, $encryption_profile);
        }
      }

      $message->content = $this->filterHtml($content);
      array_push($chat_output[$idx]->messages, $message);
    }

    return new JsonResponse(['chats' => $chat_output, 'users' => $users_with_names]);
  }

  /**
   * Inserts a new message into the database. Must belong to an active chat.
   */
  public function sendMessage(Request $request) {
    $schema_constraint = new AssertCollection([
      'fields' => [
        'chat_id' => new AssertType(['type' => 'integer']),
        'content' => new AssertType(['type' => 'string']),
        'sender' => new AssertType(['type' => 'string']),
      ],
    ]);
    $post = (array) json_decode($request->getContent());
    $is_valid = $this->isValid($post, $schema_constraint);

    if (!$is_valid) {
      return new JsonResponse(['message' => "Sending a chat message requires these fields: chat_id, content, sender."], 422);
    }

    $chat_id = $post['chat_id'];
    $content = $post['content'];
    $sender = $post['sender'];

    // Verify that the sender is the current Drupal user.
    if (!$this->verifyLoggedInUser($sender)) {
      return new JsonResponse(['message' => "Sender ID does not correspond to logged in user."], 403);
    }

    // Verify that a chat is active and that sender is a member of the chat.
    $chat = $this->database->query('SELECT active FROM ' . self::SUPPORT_CHAT_CHAT_TABLE . ' WHERE id = :chat_id AND (customer = :sender OR support_rep = :sender)', [
      ':chat_id' => $chat_id,
      ':sender' => $sender,
    ])->fetchField();

    if ($chat === FALSE) {
      return new JsonResponse(['message' => "Sender is not a member of this chat."], 400);
    }
    elseif ($chat != '1') {
      return new JsonResponse(['message' => "Chat is inactive."], 400);
    }

    if ($this->supportChatConfig->get('support_chat_use_encryption') == 1) {
      $encryption_profile = $this->supportChatConfig->get('support_chat_encryption_profile');
      $encryption_profile = $encryption_profile != NULL ? EncryptionProfile::load($encryption_profile) : NULL;

      if ($encryption_profile !== NULL) {
        $content = $this->encryptionService->encrypt($content, $encryption_profile);
      }
    }

    try {
      // Should return the message ID.
      $results = $this->database->insert(self::SUPPORT_CHAT_MSG_TABLE)
        ->fields([
          'content' => $content,
          'sender' => $sender,
          'chat_id' => $chat_id,
          'timestamp' => time(),
        ])
        ->execute();

      return (int) $results > 0
        ? new JsonResponse(['message' => "Success"])
        : new JsonResponse(['message' => "Error inserting message into database."], 500);
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
      return new JsonResponse(['message' => "Something went wrong.", 'error' => $e->getMessage()], 503);
    }
  }

  /**
   * Adds the current user (customer) to the queue in the database.
   *
   * The POST request object must contain a response to the screening question, if required by site configuration.
   */
  public function enqueue(Request $request) {
    if ($this->isQueueOpen() === FALSE) {
      return new JsonResponse(['message' => 'Queue closed', 'isOpen' => FALSE]);
    }

    $answer_required = $this->supportChatConfig->get('support_chat_screening_required') === 1;
    $schema_constraint = $answer_required === TRUE
      ? new AssertCollection([
        'fields' => [
          'customer' => new AssertType(['type' => 'string']),
          'screening_response' => new AssertType(['type' => 'string']),
        ],
      ])
      : new AssertCollection([
        'fields' => [
          'customer' => new AssertType(['type' => 'string']),
        ],
      ]);

    $user = $this->currentUser();
    $post = (array) json_decode($request->getContent());
    $is_valid = $this->isValid($post, $schema_constraint);

    if (!$is_valid) {
      return new JsonResponse([
        'message' => $answer_required === TRUE
          ? "Enqueue requires a valid customer id (customer) and response to the screening question (screening_response)."
          : "Enqueue requires a valid customer id (customer).",
      ], 422);
    }

    $customer = $post['customer'] ?? $user->id();
    $screening_response = $post['screening_response'] ?? NULL;

    if (!$this->verifyLoggedInUser($customer)) {
      return new JsonResponse(['message' => "Customer does not correspond to logged in user."], 403);
    }

    $fields = [
      'customer' => $customer,
      'screening_response' => $screening_response,
      'timestamp' => time(),
    ];

    try {
      $results = $this->database->insert(self::SUPPORT_CHAT_QUEUE_TABLE)->fields($fields)->execute();
      return (int) $results > 0 ? new JsonResponse(['message' => "Success"])
        : new JsonResponse(['message' => "Error inserting into queue table."], 500);
    }
    catch (\Exception $e) {
      return new JsonResponse([
        'message' => "Something went wrong.",
        'error' => $e->getMessage(),
      ], 500);
    }
  }

  /**
   * Removes a user from the queue. Can only be triggered by support reps.
   */
  public function dequeue(Request $request) {
    $schema_constraint = new AssertCollection([
      'fields' => [
        'customer' => new AssertType(['type' => 'string']),
      ],
    ]);

    $post = (array) json_decode($request->getContent());
    $is_valid = $this->isValid($post, $schema_constraint);

    if (!$is_valid) {
      return new JsonResponse(['message' => "Dequeue requires a valid customer id (customer)."], 422);
    }

    try {
      $results = $this->database->delete(self::SUPPORT_CHAT_QUEUE_TABLE)
        ->condition('customer', $post['customer'])->execute();

      return (int) $results > 0 ? new JsonResponse(['message' => "Success"])
        : new JsonResponse(['message' => "Error removing from queue table."], 500);

    }
    catch (\Exception $e) {
      return new JsonResponse([
        'message' => "Something went wrong.",
        'error' => $e->getMessage(),
      ], 500);
    }
  }

  /**
   * Checks if queue is currently open.
   */
  public function queueStatus() {
    $open = $this->isQueueOpen();
    return new JsonResponse(['open' => $open]);
  }

  /**
   * Returns all users currently in the SupportChat queue.
   */
  public function enumerateQueue() {
    try {
      $records = $this->database->select(self::SUPPORT_CHAT_QUEUE_TABLE, 'q')
        ->fields('q', ['customer', 'screening_response', 'timestamp'])
        ->execute()
        ->fetchAll();

      usort($records, function ($a, $b) {
        return strcmp($b->timestamp, $a->timestamp);
      });

      for ($i = 0; $i < count($records); $i++) {
        $customer = $records[$i]->customer;
        $records[$i]->customer_username = preg_match("/[a-z]+/i", $customer) > 0
          ? 'Anonymous ' . substr($customer, 0, 5)
          : $this->entityTypeManager->getStorage('user')->load(intval($customer))
          ->getUsername();
        $records[$i]->screening_response = $this->filterHtml($records[$i]->screening_response);
      }

      return new JsonResponse(['records' => $records]);
    }
    catch (\Exception $e) {
      return new JsonResponse(['message' => "Error", 500]);
    }
  }

  /**
   * Returns a customer's current position in the SupportChat queue.
   */
  public function queuePosition() {
    $current_user = (int) $this->currentUser()->id();
    if ($current_user === 0) {
      $current_user = $this->getSession($this->requestStack->getCurrentRequest());
    }

    try {
      $queue_id = $this->database->query('SELECT id FROM ' . self::SUPPORT_CHAT_QUEUE_TABLE . ' WHERE customer = :current_user', [
        ':current_user' => $current_user,
      ])->fetchField();

      if ($queue_id === FALSE) {
        return new JsonResponse(['position' => NULL]);
      }

      $position = $this->database->query('SELECT COUNT(*) FROM ' . self::SUPPORT_CHAT_QUEUE_TABLE . ' WHERE id < :queue_id', [
        ':queue_id' => $queue_id,
      ])->fetchField() + 1;

      return new JsonResponse(['position' => $position]);
    }
    catch (\Exception $e) {
      return new JsonResponse(['message' => "Error", 500]);
    }
  }

  /**
   * Creates a new chat between a customer and support rep.
   *
   * The new chat will act as a container for new messages until deactivated.
   */
  public function startChat(Request $request) {
    $schema_constraint = new AssertCollection([
      'fields' => [
        'customer' => new AssertType(['type' => 'string']),
        'support_rep' => new AssertType(['type' => 'integer']),
      ],
    ]);
    $post = (array) json_decode($request->getContent());
    $is_valid = $this->isValid($post, $schema_constraint);

    if (!$is_valid) {
      return new JsonResponse(['message' => "Starting a chat requires a customer user ID (or session) and a support rep user ID."], 422);
    }

    if (!$this->verifyLoggedInUser($post['support_rep'])) {
      return new JsonResponse(['message' => "Support rep ID does not correspond to logged in user."], 403);
    }

    try {
      // Should return the message ID.
      $results = $this->database->insert(self::SUPPORT_CHAT_CHAT_TABLE)
        ->fields([
          'customer' => $post['customer'],
          'support_rep' => $post['support_rep'],
          'active' => 1,
        ])
        ->execute();

      return (int) $results > 0
        ? new JsonResponse(['chat_id' => (int) $results])
        : new JsonResponse(['message' => "Error creating chat in database."], 500);
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
      return new JsonResponse([
        'message' => "Something went wrong.",
        'error' => $e->getMessage(),
      ], 503);
    }
  }

  /**
   * Deactivates an active chat with the chat_id provided in the request body.
   */
  public function closeChat(Request $request) {
    $schema_constraint = new AssertCollection([
      'fields' => [
        'chat_id' => new AssertType(['type' => 'integer']),
      ],
    ]);
    $put = (array) json_decode($request->getContent());
    $is_valid = $this->isValid($put, $schema_constraint);

    if (!$is_valid) {
      return new JsonResponse(['message' => "Please supply the chat ID to close the chat."], 422);
    }

    $chat_id = $put['chat_id'];

    $closer = $this->currentUser();
    if (!$closer->hasPermission('support rep')) {
      $current_user = (int) $closer->id();
      if ($current_user === 0) {
        $current_user = $this->getSession($this->requestStack->getCurrentRequest());
      }
      $chat = $this->database->query('SELECT active FROM ' . self::SUPPORT_CHAT_CHAT_TABLE . ' WHERE id = :chat_id AND (customer = :current_user OR support_rep = :current_user)', [
        ':chat_id' => $chat_id,
        ':current_user' => $current_user,
      ])->fetchField();

      if ($chat === FALSE) {
        return new JsonResponse(['message' => "Sender is not a member of this chat and does not have support rep permissions."], 400);
      }
    }

    try {
      if ($this->supportChatConfig->get('support_chat_delete_closed_chats') === 1) {
        $results = $this->database->delete(self::SUPPORT_CHAT_CHAT_TABLE)
          ->condition('id', $put['chat_id'])->execute();

        if ((int) $results > 0) {
          $chat = $this->database->query('SELECT content FROM ' . self::SUPPORT_CHAT_MSG_TABLE . ' WHERE chat_id = :chat_id', [
            ':chat_id' => $chat_id,
          ])->fetchField();

          if ($chat === FALSE) {
            return new JsonResponse(['message' => "Successfully closed chat " . $put['chat_id']]);
          }

          $results = $this->database->delete(self::SUPPORT_CHAT_MSG_TABLE)
            ->condition('chat_id', $put['chat_id'])->execute();

          return (int) $results > 0
            ? new JsonResponse(['chat_id' => "Successfully closed chat " . $put['chat_id']])
            : new JsonResponse(['message' => "Error deleting messages in database."], 500);
        }
        else {
          return new JsonResponse(['message' => "Error deleting chat in database."], 500);
        }
      }
      else {
        // Should return the message ID.
        $results = $this->database->update(self::SUPPORT_CHAT_CHAT_TABLE)
          ->fields([
            'active' => 0,
          ])
          ->condition('id', $put['chat_id'])
          ->execute();

        return (int) $results > 0
          ? new JsonResponse(['chat_id' => "Successfully closed chat " . $put['chat_id']])
          : new JsonResponse(['message' => "Error updating chat in database."], 500);
      }
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
      return new JsonResponse([
        'message' => "Something went wrong.",
        'error' => $e->getMessage(),
      ], 503);
    }
  }

  /**
   * Transfers a chat from one support rep to another.
   *
   * Can be activated by any support rep.
   */
  public function transferChat(Request $request) {
    $schema_constraint = new AssertCollection([
      'fields' => [
        'chat_id' => new AssertType(['type' => 'integer']),
        'support_rep' => new AssertType(['type' => 'integer']),
      ],
    ]);
    $put = (array) json_decode($request->getContent());
    $is_valid = $this->isValid($put, $schema_constraint);

    if (!$is_valid) {
      return new JsonResponse(['message' => "Transferring a chat requires these fields: chat_id, support_rep"], 422);
    }

    // Verify that the account corresponding to the provided ID has the support rep permission.
    $dest_user = $this->entityTypeManager->getStorage('user')->load($put['support_rep']);
    if ($dest_user === NULL) {
      return new JsonResponse(['message' => "User with provided ID not found."], 400);
    }
    if (!$dest_user->hasPermission('support rep')) {
      return new JsonResponse(['message' => "User with provided ID does not have the support rep permission."], 400);
    }

    try {
      // Should return the message ID.
      $results = $this->database->update(self::SUPPORT_CHAT_CHAT_TABLE)
        ->fields([
          'support_rep' => $put['support_rep'],
          'active' => 1,
        ])
        ->condition('id', $put['chat_id'])
        ->execute();

      return (int) $results > 0
        ? new JsonResponse(['chat_id' => "Successfully transferred chat " . $put['chat_id']])
        : new JsonResponse(['message' => "Error updating chat in database."], 500);
    }
    catch (Exception $e) {
      $this->logger->error($e->getMessage());
      return new JsonResponse([
        'message' => "Something went wrong.",
        'error' => $e->getMessage(),
      ], 503);
    }
  }

  /**
   * Allows support reps to flag themselves as available to chat.
   *
   * A potential use case is when support reps clock in/out for the day.
   *
   * @param string $availability
   *   Either "available" or "unavailable".
   *   Value determined by which endpoint is called.
   */
  public function toggleAvailability(string $availability) {
    $current_user = (int) $this->currentUser()->id();
    if ($current_user === 0) {
      return new JsonResponse(['message' => "Cannot change availability of anonymous users."], 400);
    }
    $user = $this->entityTypeManager->getStorage('user')->load($current_user);

    switch ($availability) {
      case "available":
        $user->set('field_support_chat_availability', TRUE);
        break;

      case "unavailable":
        $user->set('field_support_chat_availability', FALSE);
        break;

      default:
        return new JsonResponse(['message' => `Invalid option. Should be "available" or "unavailable".`], 400);
    }

    $user->save();
    return new JsonResponse([
      'message' => `Success`,
      'available' => $user->get('field_support_chat_availability')->value,
    ]);
  }

  /**
   * Lists all currently available support reps.
   *
   * Currently only accessible by support reps for transferring purposes.
   */
  public function availableSupportReps() {
    $support_roles = array_keys(user_role_names(FALSE, 'support rep'));

    $users = $this->entityTypeManager->getStorage('user')->getQuery()
      ->condition('roles', $support_roles, 'IN')
      ->condition('field_support_chat_availability', TRUE)
      ->execute();

    $results = [];
    foreach ($users as $user_id) {
      $user = $this->entityTypeManager->getStorage('user')->load($user_id);
      $userinfo = [
        "id" => $user_id,
        "username" => $user->getUsername(),
      ];
      $results[$user_id] = $userinfo;
    }

    return new JsonResponse(['users' => $results]);
  }

  /**
   * @} End of "defgroup chat_api_functions"
   */

  /**
   * Checks if the fields of a request body match the provided constraints.
   *
   * @param object $post
   *   The request body. Though the name is $post, could be POST or PATCH.
   * @param Symfony\Component\Validator\Constraints\Collection $constraint
   *   The AssertCollection object with field constraints.
   *
   * @return bool
   *   True if the validation constraints pass.
   */
  private function isValid($post, AssertCollection $constraint) {
    $validator = Validation::createValidator();
    $error_list = $validator->validate($post, $constraint);

    if (count($error_list) < 1) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Checks the SupportChat configuration to see if the queue is open.
   *
   * @return bool
   *   True if queue is open.
   */
  private function isQueueOpen() {
    return $this->supportChatConfig->get('support_chat_queue_open') ?? FALSE;
  }

  /**
   * Uses the session manager service to return the session for a request.
   *
   * @param Symfony\Component\HttpFoundation\Request $request
   *   Any request could be passed in, but mostly the current request object.
   *
   * @return string
   *   Returns the session ID.
   */
  private function getSession(Request $request) {
    $session_manager = $this->sessionManager;
    $started = $session_manager->start();
    $sid = $session_manager->getId();
    $session = $request->getSession();
    $session->set("chat_sid", $sid);
    return $session->get("chat_sid");
  }

  /**
   * Verifies that a session or user ID corresponds to the current user.
   *
   * @param string|int $session_or_id
   *   Numeric ID strings will be converted to int.
   *
   * @return bool
   *   True if the given ID matches the current user.
   */
  private function verifyLoggedInUser($session_or_id) {
    $id = (int) $this->currentUser()->id();
    if ($id === 0) {
      $sid = $this->getSession($this->requestStack->getCurrentRequest());
      if ($session_or_id !== $sid) {
        return FALSE;
      }
    }
    elseif (intval($session_or_id) !== $id) {
      return FALSE;
    }
    return TRUE;
  }

  /**
   * Escapes strings of any potential HTML tags. Helps avoid XSS.
   *
   * @param string $string
   *   The string to be filtered.
   *
   * @return string
   *   The filtered string.
   */
  private function filterHtml($string) {
    $string = str_replace('<', '&lt;', $string);
    $string = str_replace('>', '&gt;', $string);
    return filter_var($string, FILTER_SANITIZE_STRING);
  }

}
