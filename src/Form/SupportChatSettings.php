<?php

namespace Drupal\support_chat\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\encrypt\Entity\EncryptionProfile;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Configuration and settings form for SupportChat.
 *
 * Form is accessible at /admin/config/support_chat/configuration.
 *
 * This class also has one exposed API for queue toggling.
 */
class SupportChatSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'support_chat.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'settings_form';
  }

  /**
   * {@inheritdoc}
   *
   * Builds the SupportChat admin configuration form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('support_chat.settings');

    $seconds = [1 => 1, 2 => 2, 3 => 3, 4 => 4, 5 => 5, 6 => 6, 7 => 7, 8 => 8, 9 => 9, 10 => 10, 11 => 11, 12 => 12, 13 => 13, 14 => 14, 15 => 15, 16 => 16, 17 => 17, 18 => 18, 19 => 19, 20 => 20, 30 => 30, 40 => 40, 50 => 50, 60 => 60, 70 => 70, 80 => 80, 90 => 90, 100 => 100, 110 => 110, 120 => 120, 150 => 150, 180 => 180, 240 => 240, 300 => 300];

    $form['support_chat_general_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General Settings'),
      '#open' => TRUE,
    ];

    $form['support_chat_general_settings']['support_chat_refresh_rate'] = [
      '#type' => 'select',
      '#title' => $this->t("Polling Refresh Rate"),
      '#description' => $this->t('The time interval, in seconds, after which new messages are polled for.'),
      '#options' => $seconds,
      '#default_value' => $config->get('support_chat_refresh_rate') ?: 2,
    ];

    $form['support_chat_general_settings']['support_chat_queue_refresh_rate'] = [
      '#type' => 'select',
      '#title' => $this->t("Queue Refresh Rate"),
      '#description' => $this->t('The time interval, in seconds, in which the service window queue will be enumerated.'),
      '#options' => $seconds,
      '#default_value' => $config->get('support_chat_queue_refresh_rate') ?: 15,
    ];

    $form['support_chat_general_settings']['support_chat_delete_closed_chats'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Delete chat transcript when closed"),
      '#description' => $this->t('If unchecked, the chat will remain inactive in the database.'),
      '#default_value' => $config->get('support_chat_delete_closed_chats') ?: 0,
    ];

    $form['support_chat_general_settings']['support_chat_queue_closed_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Queue Closed Message"),
      '#description' => $this->t('The message customers will see when the queue is currently closed.'),
      '#default_value' => $config->get('support_chat_queue_closed_message') ?: NULL,
    ];

    $form['support_chat_general_settings']['support_chat_screening_required'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Require customers to answer a screening question"),
      '#description' => $this->t('If unchecked, customers will be able to enter queue without answering a question.'),
      '#default_value' => $config->get('support_chat_screening_required') ?: 0,
    ];

    $form['support_chat_general_settings']['support_chat_screening_question'] = [
      '#type' => 'textfield',
      '#title' => $this->t("Screening Question"),
      '#states' => [
        'visible' => [
          ':input[name="support_chat_screening_required"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $config->get('support_chat_screening_question') ?: NULL,
    ];

    $form['support_chat_general_settings']['support_chat_screening_question_options'] = [
      '#type' => 'textarea',
      '#title' => $this->t("Question Options"),
      '#states' => [
        'visible' => [
          ':input[name="support_chat_screening_required"]' => ['checked' => TRUE],
        ],
      ],
      '#description' => $this->t('Selectable options to the question above. Enter one per line.'),
      '#default_value' => $config->get('support_chat_screening_question_options') ?: NULL,
    ];

    $form['support_chat_path'] = [
      '#type' => 'details',
      '#title' => $this->t('SupportChat Visibility'),
      '#description' => $this->t('NOTE: Visibility settings affect customers; support rep visibility is determined by block layout.'),
      '#open' => FALSE,
    ];

    $options = [
      0 => $this->t('Everywhere'),
      1 => $this->t('Show on the listed pages only'),
      2 => $this->t('Hide on the listed pages only'),
      3 => $this->t('Disable'),
    ];

    $description = $this->t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page. Paths are case sensitive.", [
      '%blog' => '/blog',
      '%blog-wildcard' => '/blog/*',
      '%front' => '<front>',
    ]);

    $title = $this->t('Pages');

    $form['support_chat_path']['support_chat_path_visibility'] = [
      '#type' => 'radios',
      '#title' => $this->t('Show SupportChat on specific pages'),
      '#options' => $options,
      '#default_value' => $config->get('support_chat_path_visibility') ?: 0,
    ];
    $form['support_chat_path']['support_chat_path_pages'] = [
      '#type' => 'textarea',
      '#title' => '<span>' . $title . '</span>',
      '#default_value' => $config->get('support_chat_path_pages') ?: NULL,
      '#description' => $description,
    ];

    $form['support_chat_security'] = [
      '#type' => 'details',
      '#title' => $this->t('SupportChat Security'),
      '#open' => FALSE,
    ];

    $form['support_chat_security']['support_chat_use_encryption'] = [
      '#type' => 'checkbox',
      '#title' => $this->t("Encrypt messages"),
      '#description' => $this->t('In order to use encryption, you must install and configure the Encrypt and Key modules, then select an encryption profile below. More info at <a href="https://www.drupal.org/project/encrypt" target="_blank">https://www.drupal.org/project/encrypt</a>.'),
      '#default_value' => $config->get('support_chat_use_encryption') ?: 0,
    ];

    // TODO: Change this to a select field instead of autocomplete.
    $encryption_profile = $config->get('support_chat_encryption_profile');
    $encryption_profile = $encryption_profile != NULL ? EncryptionProfile::load($encryption_profile) : NULL;
    $form['support_chat_security']['support_chat_encryption_profile'] = [
      '#type' => 'entity_autocomplete',
      '#target_type' => 'encryption_profile',
      '#title' => $this->t('Encryption Profile'),
      '#states' => [
        'visible' => [
          ':input[name="support_chat_use_encryption"]' => ['checked' => TRUE],
        ],
      ],
      '#default_value' => $encryption_profile,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('support_chat.settings')
      ->set('support_chat_refresh_rate', $form_state->getValue('support_chat_refresh_rate'))
      ->set('support_chat_path_visibility', $form_state->getValue('support_chat_path_visibility'))
      ->set('support_chat_path_pages', $form_state->getValue('support_chat_path_pages'))
      ->set('support_chat_queue_refresh_rate', $form_state->getValue('support_chat_queue_refresh_rate'))
      ->set('support_chat_delete_closed_chats', $form_state->getValue('support_chat_delete_closed_chats'))
      ->set('support_chat_use_encryption', $form_state->getValue('support_chat_use_encryption'))
      ->set('support_chat_encryption_profile', $form_state->getValue('support_chat_encryption_profile'))
      ->set('support_chat_queue_closed_message', $form_state->getValue('support_chat_queue_closed_message'))
      ->set('support_chat_screening_required', $form_state->getValue('support_chat_screening_required'))
      ->set('support_chat_screening_question', $form_state->getValue('support_chat_screening_question'))
      ->set('support_chat_screening_question_options', $form_state->getValue('support_chat_screening_question_options'))
      ->save();
  }

  /**
   * Sets the queue to open or close. API exposed to support reps.
   *
   * @param string $open_or_close
   *   Either "open" or "close". Value determined by which endpoint is used.
   *
   * @ingroup chat_api_functions
   */
  public function toggleQueue(string $open_or_close) {
    $result;
    switch ($open_or_close) {
      case "open":
        $this->config('support_chat.settings')->set('support_chat_queue_open', TRUE)->save();
        $result = new JsonResponse(['message' => "Opened queue."]);
        break;

      case "close":
        $this->config('support_chat.settings')->set('support_chat_queue_open', FALSE)->save();
        $result = new JsonResponse(['message' => "Closed queue."]);
        break;

      default:
        $result = new JsonResponse(['message' => `Invalid option. Should be "open" or "close".`], 400);
    }

    return $result;
  }

}
