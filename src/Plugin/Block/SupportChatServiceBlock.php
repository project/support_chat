<?php

namespace Drupal\support_chat\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a Support Chat block for support representatives.
 *
 * @Block(
 *   id = "support_chat_service",
 *   admin_label = @Translation("Support Rep chat window"),
 *   category = @Translation("Support Chat"),
 * )
 */
class SupportChatServiceBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'support_chat_service_window',
      '#attached' => [
        'library' => [
          'support_chat/support_chat-theme-service-window',
          'support_chat/support_chat-main',
        ],
      ],
    ];
  }

}
