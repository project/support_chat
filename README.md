# SupportChat
This is a free Drupal 8 module that enables support representatives to chat
with customers on their websites. It is designed to be a plug-and-play solution
with limited configuration required. This module stands in contrast to other
Drupal 8 chat modules that either do not have the requisite features for a
customer support department or require some form of payment/subscription to use
effectively. Short polling is used to retrieve chats/messages.

Check out the project page at https://www.drupal.org/project/support_chat.

## Features
1. Chat between a customer and a support representative.
2. Screen customers' inquiries when they queue for the chat.
3. Enable support representatives to transfer chats to other active
  representatives.
4. Optionally encrypt messages using the Encrypt and Key modules.
5. Key elements of the support chat are configurable, including the following:
  - Polling refresh rates
  - Encryption
  - Screening question and selectable options

## Dependencies
- SQL database
- jQuery
- Bootstrap CSS (recommended)
- Encrypt module
- Key module

## Setup
1. After installing this module, go to
  `/admin/config/support_chat/configuration` to begin setting up the module.
  - The polling refresh rate determines how often a poll will be sent from the
    client. Currently, short polling is the only supported communication method.
  - SupportChat Visibility options determine where the *customer* will see the
    chat button.
2. If you want messages to be encrypted, follow the steps in the Encryption
  section below.
3. Give the 'Chat as Support Representative' permission to a new or existing
  role. Give the 'Access Support Chat' permission to Anonymous users,
  Authenticated users, or both, depending on whether or not you want customers
  to be logged in. (Note: If you give the 'Access Support Chat' permission to
  Anonymous users but not Authenticated users, please make sure to give your
  support representatives this permission.)
4. Add the "Support Rep chat window" block to a page; this will be the page
  that support reps access to chat with customers.

The chat queue will be closed by default; it can be opened by a support
representative or administrator. Customers cannot queue themselves if it is
closed.

To setup a custom frontend, skip step 4 above, go to the
`/admin/config/support_chat/configuration`, and select the 'Disable' option
under 'SupportChat Visibility'.

## Encryption
In order to use the encryption functionality of this module, you will need to
install the Encrypt and Key modules. Here are some recommendations when
installing/configuring these modules:

1. Install a plugin that integrates with the Encrypt module, like Real AES, in
  order to get an encryption algorithm to be used by the Encrypt module.
2. Generate a key using this command on a Unix terminal:
  `dd if=/dev/urandom bs=32 count=1 > /path/to/secret.key`
  - Store the key file on your system outside of the web root. This encourages
    real security.
3. Create a key in `/admin/config/system/keys` and point it to the file created
  in the last step.
4. Create an encryption profile in `/admin/config/system/encryption/profiles`.
  This profile will make use of whatever encryption algorithm you installed in
  step 1.
5. Go to the `/admin/config/support_chat/configuration` page and enable
  encryption, then select the newly created encryption profile.
